import CreditCard;

def initiate():
    person = {'name':'','bank':'','account_no':'','limit':''}

    print('credit card transaction operation initiate....')
    
    print('enter customer name: ')
    person['name']       = input()
    
    print('enter bank name: ')
    person['bank']       = input()
    
    print('enter account number: ')
    person['account_no'] = input()
    
    print('enter limit: ')
    person['limit']      = input()

    return person

person = initiate()

CreditCardInstance = CreditCard.CreditCard(person['name'],person['bank'],person['account_no'],person['limit']);

if __name__ == 'main':
    print(CreditCardInstance)
