class CreditCard:
    def __init__(self,customer_name,bank_name,account_no,limit):
        self.__customer_name = customer_name
        self.__bank_name     = bank_name
        self.__account_no    = account_no
        self.__limit         = float(limit)
        self.__balance       = float(0)

    def get_customer(self):
        return self.__customer_name

    def get_bank_name(self):
        return self.__bank_name

    def get_account_no(self):
        return self.__account_no

    def get_limit(self):
        return self.__limit

    def get_balance(self):
        return self.__balance

    def charge(self,charge):
        if self.__balance + charge > self.__limit:
            return False
        else:
            self.__balance += float(charge)
            return True

    def make_payment(self,amount):
        self.__balance -= amount
        return True             

